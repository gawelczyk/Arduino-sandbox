
float temperature; 
int LM35 = 0;     //sensor pin
int oneSecond = 1000;

float celcjusz1(){
  float t = analogRead(LM35); //read value from sensor
  t = t * 0.48828125; //Wyznaczenie temperatury
  return(t);
}

float celcjusz2(){
  int pin = analogRead(LM35); //read value from sensor
  float jump;
  jump = float(5) / 1023 * 1000 / 10;
  float t = pin * jump;
  //Serial.println(String("--debug-- ") + pin + "  " + String(jump, 10) + "  " + t); 
  return(t);
}

void setup() {
  Serial.begin(9600); //Prędkość transmisji w porcie szeregowym
}

void loop() {

  temperature = celcjusz2();
  Serial.print("Teperature: ");
  Serial.println(temperature);
  delay(oneSecond * 2); 
   
}
