#include <Wire.h>
#include <LiquidCrystal_I2C.h>

// Set the LCD address to 0x27 for a 16 chars and 2 line display
LiquidCrystal_I2C lcd(0x3F, 16, 2, LCD_5x10DOTS); //0x27

uint8_t heart[8] = {0x0, 0xa, 0x1f, 0x1f, 0xe, 0x4, 0x0};
float temperatura;
int LM35 = 0;
int oneSecond = 1000;

void printHello() {
  lcd.print("Hello, Adam!");
}

void print16() {
  for (int i; i < 16; i++) {
    lcd.print(i, HEX);
  }
}

void printAnia() {

  lcd.write(3);
  lcd.write(3);
  lcd.print("Hello, Ania!");
  lcd.write(3);
  lcd.write(3);
}

float celcjusz2() {
  int pin = analogRead(LM35); //Odczytanie napięcia z czujnika temperatury
  float jump;
  jump = float(5) / 1023 * 1000 / 10;
  float temperatura = pin * jump;
  //Serial.println(String("--debug-- ") + pin + "  " + String(jump, 10) + "  " + temperatura);
  return (temperatura);
}

void setup()
{
  Serial.begin(9600); //Prędkość transmisji w porcie szeregowym

  // initialize the LCD
  lcd.begin();

  lcd.createChar(3, heart);
  lcd.home();

  printHello();
  lcd.setCursor(0, 1);
  //print16();
  //printAnia();
}

void loop()
{
  //lcd.print("Hello, Adam!");
  //delay(5000);

  temperatura = celcjusz2();
  Serial.print("Temperatura: ");
  Serial.println(temperatura);

  lcd.setCursor(0, 1);
  lcd.print("Temp.: " + String(temperatura, 4));
  delay(oneSecond * 2);


}
