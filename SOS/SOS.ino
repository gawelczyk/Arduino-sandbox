
const long interval = 300;
const long longLight = 600;
const long shortLight = 300;

//const int ledPin =  LED_BUILTIN;// the number of the LED pin

const int ledPin = 12;

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(ledPin, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  for (int i=0; i < 3; i++){
    digitalWrite(ledPin, HIGH);
    delay(shortLight);
    digitalWrite(ledPin, LOW);
    delay(interval);
  }

  for (int i=0; i < 3; i++){
    digitalWrite(ledPin, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(longLight);                       // wait for a second
    digitalWrite(ledPin, LOW);    // turn the LED off by making the voltage LOW
    delay(interval);                       
  }
  
  for (int i=0; i < 3; i++){
    digitalWrite(ledPin, HIGH);  
    delay(shortLight);
    digitalWrite(ledPin, LOW);   
    delay(interval);                       
  }

  digitalWrite(ledPin, LOW);
  delay(3000);                       // wait for 3 second
  
}
