
#include <dhtnew.h>

#include <Wire.h>
#include <LiquidCrystal_I2C.h>

// Set the LCD address to 0x27 for a 16 chars and 2 line display
LiquidCrystal_I2C lcd(0x3F, 16, 2, LCD_5x10DOTS); //0x27

uint8_t heart[8] = {0x0, 0xa, 0x1f, 0x1f, 0xe, 0x4, 0x0};
float temp1;
int LM35 = 0;
int oneSecond = 1000;

int sensor2Pin = A1;
//dht sensor;
DHTNEW mySensor(A1);

void printHello() {
  lcd.print("Hello, Adam!");
}

void print16() {
  for (int i; i < 16; i++) {
    lcd.print(i, HEX);
  }
}

void printAnia() {

  lcd.write(3);
  lcd.write(3);
  lcd.print("Hello, Ania!");
  lcd.write(3);
  lcd.write(3);
}

float celcjusz2() {
  int pin = analogRead(LM35); //reading from pin
  float jump;
  jump = float(5) / 1023 * 1000 / 10;
  float t = pin * jump;
  //Serial.println(String("--debug-- ") + pin + "  " + String(jump, 10) + "  " + t);
  return (t);
}

void setup()
{
  Serial.begin(9600); //set speed

  // initialize the LCD
  lcd.begin();

  lcd.createChar(3, heart);
  lcd.home();

  printHello();
  lcd.setCursor(0, 1);
  //print16();
  //printAnia();
}

void loop()
{
  delay(oneSecond * 2); //wait a sec (recommended for DHT11)
  mySensor.read();

  temp1 = celcjusz2();
  Serial.print("temp.: ");
  Serial.println(temp1);

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Humidity = ");
  lcd.print(mySensor.humidity, 1);
  lcd.print("%");
  lcd.setCursor(0, 1);
  //lcd.print("Temp=");
  lcd.print(mySensor.temperature, 1);
  lcd.print("::" + String(temp1, 1) + " ");
  lcd.print((char)223);
  lcd.print("C");
}
