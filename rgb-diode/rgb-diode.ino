#define RED 3
#define GREEN 5
#define BLUE 6

void blinkAll(long i) {
  for (int j = 0; j < i; j++) {
    Serial.println(j);
    digitalWrite(RED, HIGH);
    digitalWrite(BLUE, HIGH);
    digitalWrite(GREEN, HIGH);
    delay(500);
    digitalWrite(RED, LOW);
    digitalWrite(BLUE, LOW);
    digitalWrite(GREEN, LOW);
    delay(500);
  }
}

void setup() {
  pinMode(RED, OUTPUT); // Piny, podłączone do diody jako wyjścia
  pinMode(GREEN, OUTPUT);
  pinMode(BLUE, OUTPUT);
  Serial.begin(9600);
}


void loop() {
  digitalWrite(RED, HIGH);
  Serial.println("RED");
  delay(2000);
  digitalWrite(RED, LOW);

  digitalWrite(GREEN, HIGH);
  Serial.println("GREEN");
  delay(2000);
  digitalWrite(GREEN, LOW);

  digitalWrite(BLUE, HIGH);
  Serial.println("BLUE");
  delay(2000);
  digitalWrite(BLUE, LOW);

  Serial.println("turn all adding one by one");
  digitalWrite(RED, HIGH);
  delay(2000);
  digitalWrite(GREEN, HIGH);
  delay(2000);
  digitalWrite(BLUE, HIGH);
  delay(4000);

  digitalWrite(RED, LOW);
  digitalWrite(BLUE, LOW);
  digitalWrite(GREEN, LOW);
  delay(4000);

  blinkAll(5);

}

